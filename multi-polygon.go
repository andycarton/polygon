package polygon

import "math"

// Multi - multipolygon
type Multi struct {
	Header                         // заголовок
	transposedBounds Bounds        // границы полигона (квадрат)
	transposer       TransposeFunc // функция транспонирования
	Vertices         []*Vertices   `json:"polygons"` // многоугльники (полигоны)
}

// NewMulti - новый составной полигон
func NewMulti(id string, rv int64, typ string, meta interface{}, deleted bool, transposer TransposeFunc, vertex ...[]Vertex) *Multi {
	p := Multi{
		Header: Header{
			ID:        id,
			RV:        rv,
			Type:      typ,
			Meta:      meta,
			IsDeleted: deleted,
		},
		transposer: transposer,
		Vertices:   make([]*Vertices, len(vertex)),
	}

	for i := range p.Vertices {
		p.Vertices[i] = NewVertices(p.transposer, vertex[i])
		p.Power += len(vertex[i]) - 1
	}

	p.Compile()
	return &p
}

// LoadMulti - создать кластер полигонов из абстракных моделей
func LoadMultiAbstract(abstract []Abstract, transposer TransposeFunc) ([]Polygoner, error) {
	var set = make([]Polygoner, 0, len(abstract))
	for i := range abstract {
		// abstract[i].Polygons
		multi := NewMulti(abstract[i].ID, abstract[i].RV, abstract[i].Type, abstract[i].Meta, abstract[i].IsDeleted, transposer)
		for _, poly := range abstract[i].Polygons {
			multi.Vertices = append(multi.Vertices, NewVertices(transposer, poly.Vertices))
			multi.Power += len(poly.Vertices) - 1
		}
		multi.Transpose(transposer)
		set = append(set, multi)
	}
	return set, nil
}

// LoadMulti - загрузить из байт, с помощью парсера
func LoadMulti(blob []byte, transposer TransposeFunc, parser ParserFunc) ([]Polygoner, error) {
	abstract, err := parser(blob)
	if err != nil {
		return nil, err
	}

	return LoadMultiAbstract(abstract, transposer)
}

// Describe - получить заголовок полигона
func (p *Multi) Describe() *Header {
	return &p.Header
}

// Compile - подготовить полигон
func (p *Multi) Compile() {
	p.Transpose(p.transposer)
}

// Transpose - Compile all vertices
func (p *Multi) Transpose(transposer TransposeFunc) {
	p.transposer = transposer
	for i := range p.Vertices {
		p.Vertices[i].Compile(p.transposer)
	}
	p.transposedBounds = p.boundingBox()
}

// boundingBox - границы составного полигона
func (p *Multi) boundingBox() Bounds {
	var bounds Bounds

	if len(p.Vertices) > 0 {
		bounds[0] = p.Vertices[0].transposedBounds[0]
		bounds[1] = p.Vertices[0].transposedBounds[1]
		bounds[2] = p.Vertices[0].transposedBounds[2]
		bounds[3] = p.Vertices[0].transposedBounds[3]
	}

	for i := range p.Vertices {
		// x
		if p.Vertices[i].transposedBounds[0] < bounds[0] {
			bounds[0] = p.Vertices[i].transposedBounds[0]
		}
		if p.Vertices[i].transposedBounds[1] > bounds[1] {
			bounds[1] = p.Vertices[i].transposedBounds[1]
		}

		// y
		if p.Vertices[i].transposedBounds[2] < bounds[2] {
			bounds[2] = p.Vertices[i].transposedBounds[2]
		}
		if p.Vertices[i].transposedBounds[3] > bounds[3] {
			bounds[3] = p.Vertices[i].transposedBounds[3]
		}
	}
	return bounds
}

// isInBoundingBox - лежит ли точка в границах составного полигона?
func (p *Multi) isInBoundingBox(vertex Vertex) bool {
	return vertex[0] >= p.transposedBounds[0] && vertex[0] <= p.transposedBounds[1] && vertex[1] >= p.transposedBounds[2] && vertex[1] <= p.transposedBounds[3]
}

// Copy - копия
func (p *Multi) Copy() Polygoner {
	c := *p
	return &c
}

// MinDistanceTo - расстояние от заданной точки до ближайшей вершины
func (p *Multi) MinDistanceTo(coordinator Coordinator) float64 {
	var v Vertex
	v[0], v[1] = p.transposer(coordinator.Lat(), coordinator.Lng())

	dist := math.Inf(1)
	for i := 0; i < len(p.Vertices); i++ {
		d := p.Vertices[i].minDistanceTo(v)
		if d < dist {
			dist = d
		}
	}

	return math.Sqrt(dist)
}

// Contains - лежит ли точка в составном полигоне?
func (p *Multi) Contains(coordinator Coordinator) bool {
	if p.IsDeleted {
		return false
	}

	var v Vertex
	v[0], v[1] = p.transposer(coordinator.Lat(), coordinator.Lng())
	if !p.isInBoundingBox(v) {
		return false
	}

	// под-полигоны могут перекрывать друг-друга, образуя дыры.
	// по этому нужно пройтись по всем
	var c = make(chan bool)
	v[0], v[1] = coordinator.Lat(), coordinator.Lng()
	for i := range p.Vertices {
		go func(i int) {
			c <- p.Vertices[i].Contains(v)
		}(i)
	}

	var contains bool
	for range p.Vertices {
		if <-c {
			contains = !contains
		}
	}

	return contains
}
