package polygon

// Header - Заголовок полигона
type Header struct {
	IsDeleted bool        `json:"isDeleted,omitempty"` // Признак удаленного многоугльника
	RV        int64       `json:"rv,omitempty"`        // Номер версии
	ID        string      `json:"id"`                  // ID многоугльника
	Type      string      `json:"type"`                // Тип
	Power     int         `json:"power"`               // Количество вершин (мощность)
	Meta      interface{} `json:"meta,omitempty"`      // Доп информация
}
