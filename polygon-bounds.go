package polygon

// Bounds - границы полигона (квадрат) (minX, maxX, minY, maxY)
type Bounds [4]float64
