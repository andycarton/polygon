package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"sync"

	"gitlab.com/andycarton/polygon"
)

type meta struct {
	Fill interface{} `json:"level"`
	Name interface{} `json:"name"`
}

func Transpose(latitude, longitude float64) (float64, float64) {
	return latitude, longitude
}

type storage struct {
	locker  sync.RWMutex
	cluster []polygon.Polygoner
}

func main() {
	set, err := polygon.LoadMulti(fixture, Transpose, YandexParser)
	if err != nil {
		fmt.Println(err)
		return
	}

	cluster := storage{cluster: set}

	fmt.Println("улица Арбат, 2/1")
	cluster.find(&polygon.Vertex{37.599419, 55.752410}) // улица Арбат, 2/1

	fmt.Println("Арбатская площадь, 14с1")
	cluster.find(&polygon.Vertex{37.602044, 55.752464}) // Арбатская площадь, 14с1

	fmt.Println("Судостроительная улица, 31к2")
	cluster.find(&polygon.Vertex{37.681325, 55.684280}) // Судостроительная улица, 31к2

	fmt.Println("улица Шухова, 10с1")
	cluster.find(&polygon.Vertex{37.611624, 55.717309}) // улица Шухова, 10с1
}

func (s *storage) find(c polygon.Coordinator) {
	defer s.locker.RUnlock()
	s.locker.RLock()

	for i := range s.cluster {
		ok := s.cluster[i].Contains(c)
		if ok {
			decribe := s.cluster[i].Describe()
			fmt.Printf(`FOUND: %#v`, decribe)
			fmt.Println()
		}
	}
	fmt.Println("-----------------")
}

func YandexParser(data []byte) ([]polygon.Abstract, error) {
	var a polygon.YandexGeoJSON
	var r []polygon.Abstract
	var ab polygon.Abstract

	err := json.Unmarshal(data, &a)
	if err != nil {
		return nil, err
	}

	for i := range a.Features {
		ab.ID = strconv.Itoa(a.Features[i].ID)
		ab.Type = a.Features[i].Geometry.Type
		ab.Polygons = make([]polygon.AbstractVertices, len(a.Features[i].Geometry.Coordinates))
		for j := range a.Features[i].Geometry.Coordinates {
			ab.Polygons[j].Vertices = a.Features[i].Geometry.Coordinates[j]
		}
		ab.Meta = meta{
			Name: a.Features[i].Properties["description"],
			Fill: a.Features[i].Properties["fill"],
		}

		r = append(r, ab)
	}

	return r, nil
}
