package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"sort"
	"strconv"
	"sync"

	"gitlab.com/andycarton/polygon"
)

type meta struct {
	Level int    `json:"level"`
	Name  string `json:"name"`
}

func ErrWrongPolygonType(id, name, key string, t interface{}) error {
	return fmt.Errorf(`wrong "%s" type "%T"`, key, t)
}

func Transpose(latitude, longitude float64) (float64, float64) {
	if longitude < -30 && longitude >= -180 {
		longitude = 360 + longitude
	}
	return latitude, longitude
}

type storage struct {
	locker  sync.RWMutex
	cluster []polygon.Polygoner
}

func main() {
	set, err := polygon.LoadMulti(fixture, Transpose, MyParser)
	if err != nil {
		fmt.Println(err)
		return
	}

	cluster := storage{cluster: set}

	fmt.Println("улица Арбат, 2/1")
	cluster.find(&polygon.Vertex{37.599419, 55.752410}) // улица Арбат, 2/1

	fmt.Println("Арбатская площадь, 14с1")
	cluster.find(&polygon.Vertex{37.602044, 55.752464}) // Арбатская площадь, 14с1

	fmt.Println("Судостроительная улица, 31к2")
	cluster.find(&polygon.Vertex{37.681325, 55.684280}) // Судостроительная улица, 31к2

	fmt.Println("улица Шухова, 10с1")
	cluster.find(&polygon.Vertex{37.611624, 55.717309}) // улица Шухова, 10с1
}

func (s *storage) find(c polygon.Coordinator) {
	defer s.locker.RUnlock()
	s.locker.RLock()

	for i := range s.cluster {
		ok := s.cluster[i].Contains(c)
		if ok {
			decribe := s.cluster[i].Describe()
			fmt.Printf(`FOUND: %#v`, decribe)
			fmt.Println()
		}
	}
	fmt.Println("-----------------")
}

func MyParser(data []byte) ([]polygon.Abstract, error) {
	var a []interface{}

	err := json.Unmarshal(data, &a)
	if err != nil {
		return nil, err
	}

	var p []polygon.Abstract
	var ap polygon.Abstract
	var m meta
	var vertex polygon.Vertex
	var av polygon.AbstractVertices

	for i := range a {
		if node, ok := a[i].(map[string]interface{}); ok {

			keys := make([]string, len(node))
			for k := range node {
				keys = append(keys, k)
			}
			sort.Slice(keys, func(i, j int) bool {
				return keys[i] > keys[j]
			})

			ap.ID = ""
			m.Name = ""

			for _, k := range keys {
				v := node[k]

				switch k {
				case "id":
					pl, ok := v.(float64)
					if !ok {
						return nil, ErrWrongPolygonType(ap.ID, m.Name, k, v)
					}
					ap.ID = strconv.FormatInt(int64(pl), 5)

				case "level":
					pl, ok := v.(float64)
					if !ok {
						return nil, ErrWrongPolygonType(ap.ID, m.Name, k, v)
					}
					m.Level = int(int64(pl))

				case "name":
					pl, ok := v.(string)
					if !ok {
						return nil, ErrWrongPolygonType(ap.ID, m.Name, k, v)
					}
					m.Name = pl

				case "geometry":
					pl, ok := v.(map[string]interface{})
					if !ok {
						return nil, ErrWrongPolygonType(ap.ID, m.Name, k, v)
					}

					ap.Power = 0
					ap.Polygons = make([]polygon.AbstractVertices, 0, 20)

					for kp := range pl {
						switch kp {
						case "type":
							plp, ok := pl[kp].(string)
							if !ok {
								return nil, ErrWrongPolygonType(ap.ID, m.Name, k+"."+kp, pl[kp])
							}

							if plp != "MultiPolygon" {
								return nil, errors.New(`wrong polygon type "` + plp + `"`)
							}
							ap.Type = plp
						case "coordinates":
							plp, ok := pl[kp].([]interface{})
							if !ok {
								return nil, ErrWrongPolygonType(ap.ID, m.Name, k+"."+kp, pl[kp])
							}

							for i := range plp {
								plp, ok := plp[i].([]interface{})
								if !ok {
									return nil, ErrWrongPolygonType(ap.ID, m.Name, k+"."+kp, plp)
								}

								for ii := range plp {
									plp, ok := plp[ii].([]interface{})
									if !ok {
										return nil, ErrWrongPolygonType(ap.ID, m.Name, k+"."+kp, plp)
									}

									// вершины полигона
									var vertices []polygon.Vertex
									for iii := range plp {
										plp, ok := plp[iii].([]interface{})
										if !ok {
											return nil, ErrWrongPolygonType(ap.ID, m.Name, k+"."+kp, plp)
										}

										if len(plp) != 2 {
											return nil, ErrWrongPolygonType(ap.ID, m.Name, k+"."+kp, plp)
										}

										// координаты вершины
										vertex[0], ok = plp[0].(float64)
										if !ok {
											return nil, ErrWrongPolygonType(ap.ID, m.Name, k+"."+kp+" lat", plp[0])
										}
										vertex[1], ok = plp[1].(float64)
										if !ok {
											return nil, ErrWrongPolygonType(ap.ID, m.Name, k+"."+kp+" lng", plp[0])
										}

										// добавление вершины полигона
										vertices = append(vertices, vertex)
									}

									var id = strconv.Itoa(i+1) + "." + strconv.Itoa(ii+1)

									av.ID = ap.ID + ":" + id
									av.Type = "Polygon"
									av.Power = len(vertices) - 1
									av.Vertices = vertices
									av.Meta = struct {
										Name string `json:"name"`
									}{
										Name: m.Name + " " + id,
									}

									ap.Power = ap.Power + len(vertices) - 1
									ap.Polygons = append(ap.Polygons, av)
								}
							}
						default:
						}
					}
				}
			}

			ap.Meta = m
			p = append(p, ap)
		}
	}

	return p, nil
}
