package polygon

import (
	"encoding/json"
	"strconv"
	"sync"
	"testing"
)

var set, err = LoadMulti(fixture, Transpose, YandexParser)
var cluster = storage{cluster: set}
var v = &Vertex{37.599419, 55.752410} // улица Арбат, 2/1

func BenchmarkFind(b *testing.B) {
	for i := 0; i < b.N; i++ {
		cluster.find(v)
	}
}
func BenchmarkFindParallel(b *testing.B) {
	for i := 0; i < b.N; i++ {
		cluster.findParallel(v)
	}
}

type meta struct {
	Fill interface{} `json:"level"`
	Name interface{} `json:"name"`
}

func Transpose(latitude, longitude float64) (float64, float64) {
	return latitude, longitude
}

type storage struct {
	locker  sync.RWMutex
	cluster []Polygoner
}

func (s *storage) find(c Coordinator) []*Header {
	var res = make([]*Header, 0, 5)

	defer s.locker.RUnlock()
	s.locker.RLock()

	for i := range s.cluster {
		if s.cluster[i].Contains(c) {
			res = append(res, s.cluster[i].Describe())
		}
	}
	return res
}

func (s *storage) findParallel(c Coordinator) []*Header {
	var res = make([]*Header, 0, 5)
	var wg sync.WaitGroup

	defer s.locker.RUnlock()
	s.locker.RLock()

	wg.Add(len(s.cluster))
	for i := range s.cluster {
		go func(i int) {
			if s.cluster[i].Contains(c) {
				res = append(res, s.cluster[i].Describe())
			}
			wg.Done()
		}(i)
	}
	wg.Wait()

	return res
}

func YandexParser(data []byte) ([]Abstract, error) {
	var r []Abstract
	var ab Abstract
	var a YandexGeoJSON

	err := json.Unmarshal(data, &a)
	if err != nil {
		return nil, err
	}

	for i := range a.Features {
		ab.ID = strconv.Itoa(a.Features[i].ID)
		ab.Type = a.Features[i].Geometry.Type
		ab.Polygons = make([]AbstractVertices, len(a.Features[i].Geometry.Coordinates))
		for j := range a.Features[i].Geometry.Coordinates {
			ab.Polygons[j].Vertices = a.Features[i].Geometry.Coordinates[j]
		}
		ab.Meta = meta{
			Name: a.Features[i].Properties["description"],
			Fill: a.Features[i].Properties["fill"],
		}

		r = append(r, ab)
	}

	return r, nil
}
