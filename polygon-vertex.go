package polygon

// Vertex - пара координат. 0 - lat 1 - lng
type Vertex [2]float64 // lat lng

// Lat .
func (v *Vertex) Lat() float64 {
	return v[0]
}

// Lng .
func (v *Vertex) Lng() float64 {
	return v[1]
}

// Transpose .
func (v *Vertex) Transpose(transpose TransposeFunc) Coordinator {
	var t Vertex
	t[0], t[1] = transpose(v.Lat(), v.Lng())
	return &t
}
