package polygon

// YandexGeoJSON .
type YandexGeoJSON struct {
	Type     string                 `json:"type"`
	Metadata YandexGeoJSONMetadata  `json:"metadata"`
	Features []YandexGeoJSONFeature `json:"features"`
}

// YandexGeoJSONMetadata .
type YandexGeoJSONMetadata struct {
	Creator string `json:"creator"`
	Name    string `json:"name"`
}

// YandexGeoJSONFeature .
type YandexGeoJSONFeature struct {
	ID         int                     `json:"id"`
	Type       string                  `json:"type"`
	Geometry   YandexGeoJSONGeometry   `json:"geometry"`
	Properties YandexGeoJSONProperties `json:"properties"`
}

// YandexGeoJSONGeometry .
type YandexGeoJSONGeometry struct {
	Type        string     `json:"type"`
	Coordinates [][]Vertex `json:"coordinates"`
}

// YandexGeoJSONProperties .
type YandexGeoJSONProperties map[string]interface{}
