package polygon

import (
	"math"
	"testing"
)

func transpose(latitude, longitude float64) (float64, float64) {
	if longitude < -30 && longitude >= -180 {
		longitude = 360 + longitude
	}
	return latitude, longitude
}

func TestMulti_Contains(t *testing.T) {
	type fields struct {
		vertices [][]Vertex
	}
	type args struct {
		vertex Vertex
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "empty polygon",
			args: args{vertex: Vertex{0, 0}},
			fields: fields{vertices: [][]Vertex{
				{},
			}},
			want: false,
		},
		{
			name: "inside one 1",
			args: args{vertex: Vertex{0, 0}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
			}},
			want: true,
		},
		{
			name: "inside first",
			args: args{vertex: Vertex{0, 0}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: true,
		},
		{
			name: "inside second",
			args: args{vertex: Vertex{-3, 3}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: true,
		},
		{
			name: "in vertex 1",
			args: args{vertex: Vertex{-4, 4}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: true,
		},
		{
			name: "in vertex 2",
			args: args{vertex: Vertex{-2, 4}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: true,
		},
		{
			name: "in vertex 3",
			args: args{vertex: Vertex{-4, 2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: true,
		},
		{
			name: "in vertex 4 (in a hole)",
			args: args{vertex: Vertex{-2, 2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: false,
		},
		{
			name: "in vertex 5",
			args: args{vertex: Vertex{-2, -2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: true,
		},
		{
			name: "in vertex 6",
			args: args{vertex: Vertex{2, -2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: true,
		},
		{
			name: "in vertex 7",
			args: args{vertex: Vertex{2, 2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: true,
		},
		{
			name: "on border line 1 top",
			args: args{vertex: Vertex{-3, 4}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: false,
		},
		{
			name: "on border line 2 right",
			args: args{vertex: Vertex{-2, 3}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: false,
		},
		{
			name: "on border line 3",
			args: args{vertex: Vertex{-3, 2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: true,
		},
		{
			name: "on border line 4 left",
			args: args{vertex: Vertex{-4, 3}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: true,
		},
		{
			name: "on border line 5 top",
			args: args{vertex: Vertex{0, 2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: false,
		},
		{
			name: "on border line 6 right",
			args: args{vertex: Vertex{2, 0}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: false,
		},
		{
			name: "on border line 7 bottom",
			args: args{vertex: Vertex{0, -2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: true,
		},
		{
			name: "on border line 8 left",
			args: args{vertex: Vertex{-2, 0}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: true,
		},
		{
			name: "on border line 9 left top",
			args: args{vertex: Vertex{-2, 2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
			}},
			want: true,
		},
		{
			name: "on border line 10 top right",
			args: args{vertex: Vertex{1, 2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
			}},
			want: false,
		},
		{
			name: "on border line 11",
			args: args{vertex: Vertex{2, 0}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
			}},
			want: true,
		},
		{
			name: "on border line 12 bottom right",
			args: args{vertex: Vertex{2, 1}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
			}},
			want: false,
		},
		{
			name: "on border line 13",
			args: args{vertex: Vertex{-2, 1.5}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
			}},
			want: true,
		},
		{
			name: "in side one 2",
			args: args{vertex: Vertex{0, 0}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
			}},
			want: true,
		},
		{
			name: "in side one 3",
			args: args{vertex: Vertex{-1, 1}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
			}},
			want: true,
		},
		{
			name: "in side one 4",
			args: args{vertex: Vertex{1, 1}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
			}},
			want: true,
		},
		{
			name: "inside two 1",
			args: args{vertex: Vertex{-3, 0}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
				{Vertex{-1, 2}, Vertex{1, 1}, Vertex{1, -1}, Vertex{-2, -1}, Vertex{-2, 1}, Vertex{-1, 2}},
			}},
			want: true,
		},
		{
			name: "inside two 2",
			args: args{vertex: Vertex{0, 2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
				{Vertex{-1, 2}, Vertex{1, 1}, Vertex{1, -1}, Vertex{-2, -1}, Vertex{-2, 1}, Vertex{-1, 2}},
			}},
			want: true,
		},
		{
			name: "inside two 3",
			args: args{vertex: Vertex{1, 0}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
				{Vertex{-1, 2}, Vertex{1, 1}, Vertex{1, -1}, Vertex{-2, -1}, Vertex{-2, 1}, Vertex{-1, 2}},
			}},
			want: true,
		},
		{
			name: "inside two 4",
			args: args{vertex: Vertex{-1, -2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
				{Vertex{-1, 2}, Vertex{1, 1}, Vertex{1, -1}, Vertex{-2, -1}, Vertex{-2, 1}, Vertex{-1, 2}},
			}},
			want: true,
		},
		{
			name: "in a hole 1",
			args: args{vertex: Vertex{1, 1}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
				{Vertex{-1, 2}, Vertex{1, 1}, Vertex{1, -1}, Vertex{-2, -1}, Vertex{-2, 1}, Vertex{-1, 2}},
			}},
			want: false,
		},
		{
			name: "in a hole 2",
			args: args{vertex: Vertex{0.5, -0.5}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
				{Vertex{-1, 2}, Vertex{1, 1}, Vertex{1, -1}, Vertex{-2, -1}, Vertex{-2, 1}, Vertex{-1, 2}},
			}},
			want: false,
		},
		{
			name: "in a hole 3 from three poly",
			args: args{vertex: Vertex{-0.5, -0.5}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
				{Vertex{-1, 2}, Vertex{1, 1}, Vertex{1, -1}, Vertex{-2, -1}, Vertex{-2, 1}, Vertex{-1, 2}},
				{Vertex{-1, 1}, Vertex{0, 1}, Vertex{0, 0}, Vertex{-1, 0}, Vertex{-1, 1}},
			}},
			want: false,
		},
		{
			name: "on the island 1 center",
			args: args{vertex: Vertex{-0.5, 0.5}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
				{Vertex{-1, 2}, Vertex{1, 1}, Vertex{1, -1}, Vertex{-2, -1}, Vertex{-2, 1}, Vertex{-1, 2}},
				{Vertex{-1, 1}, Vertex{0, 1}, Vertex{0, 0}, Vertex{-1, 0}, Vertex{-1, 1}},
			}},
			want: true,
		},
		{
			name: "on the island 2 center top border",
			args: args{vertex: Vertex{-0.5, 1}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
				{Vertex{-1, 2}, Vertex{1, 1}, Vertex{1, -1}, Vertex{-2, -1}, Vertex{-2, 1}, Vertex{-1, 2}},
				{Vertex{-1, 1}, Vertex{0, 1}, Vertex{0, 0}, Vertex{-1, 0}, Vertex{-1, 1}},
			}},
			want: false,
		},
		{
			name: "on the island 2 center bottom border",
			args: args{vertex: Vertex{-0.5, 0}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
				{Vertex{-1, 2}, Vertex{1, 1}, Vertex{1, -1}, Vertex{-2, -1}, Vertex{-2, 1}, Vertex{-1, 2}},
				{Vertex{-1, 1}, Vertex{0, 1}, Vertex{0, 0}, Vertex{-1, 0}, Vertex{-1, 1}},
			}},
			want: true,
		},
		{
			name: "on the island 2 center right border",
			args: args{vertex: Vertex{0, 0.5}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
				{Vertex{-1, 2}, Vertex{1, 1}, Vertex{1, -1}, Vertex{-2, -1}, Vertex{-2, 1}, Vertex{-1, 2}},
				{Vertex{-1, 1}, Vertex{0, 1}, Vertex{0, 0}, Vertex{-1, 0}, Vertex{-1, 1}},
			}},
			want: false,
		},
		{
			name: "on the island 2 center left border",
			args: args{vertex: Vertex{-1, 0.5}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-4, 0}, Vertex{0, 4}, Vertex{2, 0}, Vertex{4, 0}, Vertex{0, -2}, Vertex{0, -3}, Vertex{-4, 0}},
				{Vertex{-1, 2}, Vertex{1, 1}, Vertex{1, -1}, Vertex{-2, -1}, Vertex{-2, 1}, Vertex{-1, 2}},
				{Vertex{-1, 1}, Vertex{0, 1}, Vertex{0, 0}, Vertex{-1, 0}, Vertex{-1, 1}},
			}},
			want: true,
		},
		{
			name: "in a hole 3",
			args: args{vertex: Vertex{-2, 2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-3, 3}, Vertex{3, 3}, Vertex{3, -3}, Vertex{-3, -3}, Vertex{-3, 3}},
				{Vertex{-4, 4}, Vertex{-1, 4}, Vertex{-1, 1}, Vertex{-4, 1}, Vertex{-4, 4}},
			}},
			want: false,
		},
		{
			name: "in vertex in a hole",
			args: args{vertex: Vertex{-2, 2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: false,
		},
		{
			name: "outside 1",
			args: args{vertex: Vertex{-4.00001, 3}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: false,
		},
		{
			name: "outside 2",
			args: args{vertex: Vertex{-4.00001, 4.00001}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: false,
		},
		{
			name: "outside 3",
			args: args{vertex: Vertex{-1.99999, 4.00001}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: false,
		},
		{
			name: "outside 4",
			args: args{vertex: Vertex{-1.99999, 2.00001}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: false,
		},
	}

	type meta struct {
		OfficeID int `json:"officeID"`
	}
	m := meta{
		OfficeID: 12,
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewMulti("0", 0, "MultiPolygon", &m, false, transpose, tt.fields.vertices...)

			if got := p.Contains(&tt.args.vertex); got != tt.want {
				t.Errorf("Multi.Contains() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMulti_MinDistanceTo(t *testing.T) {
	type fields struct {
		vertices [][]Vertex
	}
	type args struct {
		vertex Vertex
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   float64
	}{
		{
			name: "empty polygon",
			args: args{vertex: Vertex{0, 0}},
			fields: fields{vertices: [][]Vertex{
				{},
			}},
			want: math.Inf(1),
		},
		{
			name: "one polygon on line",
			args: args{vertex: Vertex{0, 2}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
			}},
			want: 2,
		},
		{
			name: "one polygon 1",
			args: args{vertex: Vertex{-2, 3}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
			}},
			want: 1,
		},
		{
			name: "one polygon 2",
			args: args{vertex: Vertex{-5, 6}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
			}},
			want: 5,
		},
		{
			name: "multi polygon on vertex 1",
			args: args{vertex: Vertex{-2, 4}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
		},
		{
			name: "multi polygon on vertex 2",
			args: args{vertex: Vertex{-8, 7}},
			fields: fields{vertices: [][]Vertex{
				{Vertex{-2, 2}, Vertex{2, 2}, Vertex{2, -2}, Vertex{-2, -2}, Vertex{-2, 2}},
				{Vertex{-4, 4}, Vertex{-2, 4}, Vertex{-2, 2}, Vertex{-4, 2}, Vertex{-4, 4}},
			}},
			want: 5,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewMulti("0", 0, "MultiPolygon", nil, false, transpose, tt.fields.vertices...)

			if got := p.MinDistanceTo(&tt.args.vertex); got != tt.want {
				t.Errorf("Multi.MinDistanceTo() = %v, want %v", got, tt.want)
			}
		})
	}
}
