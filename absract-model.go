package polygon

// Abstract - абстрактная модель составного полигона
type Abstract struct {
	Header
	Polygons []AbstractVertices `json:"polygons"` // Многоугльники
}

// AbstractVertices - абстрактная модель полигона
type AbstractVertices struct {
	Header
	Vertices []Vertex `json:"vertices"` // Вершины многоугльника
}
