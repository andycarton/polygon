package polygon

import "math"

// Vertices .
type Vertices struct {
	Source           []Vertex      `json:"vertices"` // исходные вершины
	transposer       TransposeFunc // функция транспонирования
	transposed       []Vertex      // транспонированные вершины
	transposedBounds Bounds        // транспонированные границы полигона (квадрат)
}

// NewVertices - новый полигон
func NewVertices(transposer TransposeFunc, vertices []Vertex) *Vertices {
	v := Vertices{
		Source:     vertices,
		transposer: transposer,
	}

	v.Compile(transposer)
	return &v
}

// Copy - копия
func (v *Vertices) Copy() *Vertices {
	c := *v
	return &c
}

// MinDistanceTo - расстояние от заданной точки до ближайшей вершины
func (v *Vertices) MinDistanceTo(coordinator Coordinator) float64 {
	var p Vertex
	p[0], p[1] = v.transposer(coordinator.Lat(), coordinator.Lng())

	return math.Sqrt(v.minDistanceTo(p))
}

// MinDistanceTo - расстояние (квадратичное) от заданной точки до ближайшей вершины
func (v *Vertices) minDistanceTo(p Vertex) float64 {
	dist := math.Inf(1)
	if len(v.transposed) == 0 {
		return dist
	}

	if len(v.transposed) == 1 {
		return math.Sqrt((p[0]-v.transposed[0][0])*(p[0]-v.transposed[0][0]) + (p[1]-v.transposed[0][1])*(p[1]-v.transposed[0][1]))
	}

	for i := 0; i < len(v.transposed)-1; i++ {
		d := (p[0]-v.transposed[i][0])*(p[0]-v.transposed[i][0]) + (p[1]-v.transposed[i][1])*(p[1]-v.transposed[i][1])
		if d < dist {
			dist = d
		}
	}

	return dist
}

// Compile - подготовка полигонов
func (v *Vertices) Compile(transposer TransposeFunc) {
	if len(v.transposed) != len(v.Source) {
		v.transposed = make([]Vertex, len(v.Source))
	}

	v.transposer = transposer

	// танспанируем точки
	t := Vertex{}
	for i := range v.Source {
		t[0], t[1] = transposer(v.Source[i][0], v.Source[i][1])
		v.transposed[i] = t
	}

	v.transposedBounds = v.boundingBox(v.transposed)
}

// boundingBox - границы полигона
func (v *Vertices) boundingBox(vertices []Vertex) Bounds {
	var res Bounds

	if len(vertices) == 0 {
		return res
	}

	res[0] = vertices[0][0]
	res[1] = vertices[0][0]
	res[2] = vertices[0][1]
	res[3] = vertices[0][1]

	for i := range vertices {
		// x
		if vertices[i][0] < res[0] {
			res[0] = vertices[i][0]
		}
		if vertices[i][0] > res[1] {
			res[1] = vertices[i][0]
		}

		// y
		if vertices[i][1] < res[2] {
			res[2] = vertices[i][1]
		}
		if vertices[i][1] > res[3] {
			res[3] = vertices[i][1]
		}
	}

	return res
}

// isInBoundingBox - находится ли точка в границах полигона?
func (v *Vertices) isInBoundingBox(vertex Vertex) bool {
	return vertex[0] >= v.transposedBounds[0] && vertex[0] <= v.transposedBounds[1] && vertex[1] >= v.transposedBounds[2] && vertex[1] <= v.transposedBounds[3]
}

// Contains - лежит ли точка в полигоне?
func (v *Vertices) Contains(vertex Vertex) bool {
	vertex[0], vertex[1] = v.transposer(vertex[0], vertex[1])

	if !v.isInBoundingBox(vertex) {
		return false
	}

	var intersect bool
	for i, j := 1, 0; i < len(v.transposed); j, i = i, i+1 {
		// точка является одной из вершин
		if v.transposed[j][0] == vertex[0] && v.transposed[j][1] == vertex[1] {
			return true
		}

		// самый простой алгоритм без тригонометрии
		if ((v.transposed[i][1] > vertex[1]) != (v.transposed[j][1] > vertex[1])) && vertex[0] < (v.transposed[j][0]-v.transposed[i][0])*(vertex[1]-v.transposed[i][1])/(v.transposed[j][1]-v.transposed[i][1])+v.transposed[i][0] {
			intersect = !intersect
			continue
		}
	}

	return intersect
}
