package polygon

// TransposeFunc - нотация функции транспонирования
type TransposeFunc func(latitude, longitude float64) (float64, float64)

// ParserFunc - нотаци функции парсера
type ParserFunc func([]byte) ([]Abstract, error)

// Coordinator .
type Coordinator interface {
	Lat() float64
	Lng() float64
}

// Coordinator .
type Polygoner interface {
	Contains(Coordinator) bool
	Transpose(TransposeFunc)
	Describe() *Header
	Copy() Polygoner
}
